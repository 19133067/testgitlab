﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALayer;
using System.Data;
using System.Data.SqlClient;

namespace BLLayer
{
    public class BLL
    {
        DAL db;

        public BLL()
        {
            db = new DAL();
        }
        public DataSet getKhachHang()
        {
            return db.ExecuteQueryDataset("select MaKH,HoTen,CMND from KhachThue;", CommandType.Text, null);

        }
        public DataSet getHopDong()
        {
            return db.ExecuteQueryDataset("select KhachThue.MaKH,KhachThue.HoTen,ChiTietHopDongThue.MaPhong,HopDongThuePhong.SoHD, HopDongThuePhong.NgayThue, HopDongThuePhong.TriGiaHD,HopDongThuePhong.TinhTrangHD, HopDongThuePhong.MaNV, ChiTietHopDongThue.GiaThue from KhachThue INNER JOIN HopDongThuePhong ON KhachThue.MaKH = HopDongThuePhong.MaKH INNER JOIN ChiTietHopDongThue on HopDongThuePhong.SoHD = ChiTietHopDongThue.SoHD; ", CommandType.Text, null);

        }
        public bool insertKhachThue (ref string err, string MaKH, string HoTen, string CMND)
        {
            return db.MyExecuteNonQuery("spInsertKhachThue", CommandType.StoredProcedure, ref err, new SqlParameter("@MaKH", MaKH), new SqlParameter("@HoTen", HoTen), new SqlParameter("@CMND", CMND));
        }
        public bool suuKhachThue(ref string err, string MaKH, string HoTen, string CMND)
        {
            return db.MyExecuteNonQuery("spSuaKhachThue", CommandType.StoredProcedure, ref err, new SqlParameter("@MaKH", MaKH), new SqlParameter("@HoTen", HoTen), new SqlParameter("@CMND", CMND));
        }
        public bool xoaKhachThue(ref string err, string MaKH)
        {
            return db.MyExecuteNonQuery("spXoaKhachThue", CommandType.StoredProcedure, ref err, new SqlParameter("@MaKH", MaKH));
        }
        public DataSet getPhongTro()
        {
            return db.ExecuteQueryDataset("Select PhongTro.MaPhong,PhongTro.SoPhong,PhongTro.MaLoaiPhong,PhongTro.TinhTrang,LoaiPhong.TenLoaiPhong, BangGia.GiaThue, BangGia.Ngay, ThietBi.MaTB, ThietBi.tenTB,TrangBi.NgayTrangBi, TrangBi.SoLuong from(PhongTro INNER JOIN LoaiPhong on PhongTro.MaLoaiPhong = LoaiPhong.MaLoaiPhong INNER JOIN BangGia on PhongTro.MaPhong = BangGia.MaPhong INNER JOIN TrangBi on PhongTro.MaPhong = TrangBi.MaPhong INNER JOIN ThietBi on ThietBi.MaTB = TrangBi.MaTB); ", CommandType.Text,null);
        }
        public bool insertPhongTro(ref string err, string MapHong, string SoPhong, string TinhTrang, string MaLoaiPhong, string TenLoaiPhong, DateTime Ngay, string GiaThue)
        {
            return db.MyExecuteNonQuery("spInsertPhongTro", CommandType.StoredProcedure, ref err, new SqlParameter("@MaPhong",MapHong), new SqlParameter("@SoPhong", SoPhong), new SqlParameter("@TinhTrang",TinhTrang), new SqlParameter("@MaLoaiPhong", MaLoaiPhong), new SqlParameter("@TenLoaiPhong", TenLoaiPhong), new SqlParameter("@Ngay", Ngay),new SqlParameter("@GiaThue",GiaThue));
        }
    }
}
