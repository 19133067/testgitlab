﻿
namespace PhongTro
{
    partial class HopDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dvgHopDong = new System.Windows.Forms.DataGridView();
            this.MaKH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaPhong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoHD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TriGiaHD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TinhTrangHD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaNV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GiaThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnThoat2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dvgHopDong)).BeginInit();
            this.SuspendLayout();
            // 
            // dvgHopDong
            // 
            this.dvgHopDong.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgHopDong.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaKH,
            this.HoTen,
            this.MaPhong,
            this.SoHD,
            this.NgayThue,
            this.TriGiaHD,
            this.TinhTrangHD,
            this.MaNV,
            this.GiaThue});
            this.dvgHopDong.Location = new System.Drawing.Point(35, 21);
            this.dvgHopDong.Name = "dvgHopDong";
            this.dvgHopDong.RowHeadersWidth = 51;
            this.dvgHopDong.RowTemplate.Height = 24;
            this.dvgHopDong.Size = new System.Drawing.Size(839, 454);
            this.dvgHopDong.TabIndex = 0;
            // 
            // MaKH
            // 
            this.MaKH.DataPropertyName = "MaKH";
            this.MaKH.Frozen = true;
            this.MaKH.HeaderText = "MaKH";
            this.MaKH.MinimumWidth = 6;
            this.MaKH.Name = "MaKH";
            this.MaKH.Width = 125;
            // 
            // HoTen
            // 
            this.HoTen.DataPropertyName = "HoTen";
            this.HoTen.Frozen = true;
            this.HoTen.HeaderText = "Họ Tên";
            this.HoTen.MinimumWidth = 6;
            this.HoTen.Name = "HoTen";
            this.HoTen.Width = 125;
            // 
            // MaPhong
            // 
            this.MaPhong.DataPropertyName = "MaPhong";
            this.MaPhong.HeaderText = "Mã Phòng";
            this.MaPhong.MinimumWidth = 6;
            this.MaPhong.Name = "MaPhong";
            this.MaPhong.Width = 125;
            // 
            // SoHD
            // 
            this.SoHD.DataPropertyName = "SoHD";
            this.SoHD.HeaderText = "Số HD";
            this.SoHD.MinimumWidth = 6;
            this.SoHD.Name = "SoHD";
            this.SoHD.Width = 125;
            // 
            // NgayThue
            // 
            this.NgayThue.DataPropertyName = "NgayThue";
            this.NgayThue.HeaderText = "Ngày Thuê";
            this.NgayThue.MinimumWidth = 6;
            this.NgayThue.Name = "NgayThue";
            this.NgayThue.Width = 125;
            // 
            // TriGiaHD
            // 
            this.TriGiaHD.DataPropertyName = "TriGiaHD";
            this.TriGiaHD.HeaderText = "Trị Giá HD";
            this.TriGiaHD.MinimumWidth = 6;
            this.TriGiaHD.Name = "TriGiaHD";
            this.TriGiaHD.Width = 125;
            // 
            // TinhTrangHD
            // 
            this.TinhTrangHD.DataPropertyName = "TinhTrangHD";
            this.TinhTrangHD.HeaderText = "Tình Trạng HĐ";
            this.TinhTrangHD.MinimumWidth = 6;
            this.TinhTrangHD.Name = "TinhTrangHD";
            this.TinhTrangHD.Width = 125;
            // 
            // MaNV
            // 
            this.MaNV.DataPropertyName = "MaNV";
            this.MaNV.HeaderText = "Mã NV";
            this.MaNV.MinimumWidth = 6;
            this.MaNV.Name = "MaNV";
            this.MaNV.Width = 125;
            // 
            // GiaThue
            // 
            this.GiaThue.DataPropertyName = "GiaThue";
            this.GiaThue.HeaderText = "GiaThue";
            this.GiaThue.MinimumWidth = 6;
            this.GiaThue.Name = "GiaThue";
            this.GiaThue.Width = 125;
            // 
            // btnThoat2
            // 
            this.btnThoat2.Location = new System.Drawing.Point(799, 481);
            this.btnThoat2.Name = "btnThoat2";
            this.btnThoat2.Size = new System.Drawing.Size(75, 45);
            this.btnThoat2.TabIndex = 1;
            this.btnThoat2.Text = "Thoát";
            this.btnThoat2.UseVisualStyleBackColor = true;
            this.btnThoat2.Click += new System.EventHandler(this.btnThoat2_Click);
            // 
            // HopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 538);
            this.Controls.Add(this.btnThoat2);
            this.Controls.Add(this.dvgHopDong);
            this.Name = "HopDong";
            this.Text = "Chi tiết hợp đồng";
            this.Load += new System.EventHandler(this.HopDong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvgHopDong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dvgHopDong;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaKH;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaPhong;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayThue;
        private System.Windows.Forms.DataGridViewTextBoxColumn TriGiaHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn TinhTrangHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaNV;
        private System.Windows.Forms.DataGridViewTextBoxColumn GiaThue;
        private System.Windows.Forms.Button btnThoat2;
    }
}