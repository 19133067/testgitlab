﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLayer;
using DALayer;
using System.Data;
using System.Data.SqlClient;


namespace PhongTro
{
    public partial class HopDong : Form
    {
        DataSet ds = new DataSet();
        BLL hopdong = new BLL();
        public HopDong()
        {
            InitializeComponent();
        }

        private void DataBindHopDong()
        {

            ds = hopdong.getHopDong();
            dvgHopDong.DataSource = ds.Tables[0];
        }

        private void HopDong_Load(object sender, EventArgs e)
        {
            DataBindHopDong();
        }

        private void btnThoat2_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có chắc chắn muốn thoát ?", "Trả lời",
            MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                this.Close();
            }
        }
    }
}
