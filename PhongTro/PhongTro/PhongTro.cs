﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLayer;
using DALayer;
using System.Data;
using System.Data.SqlClient;

namespace PhongTro
{
    public partial class PhongTro : Form
    {
        bool them;
        DAL da = new DAL();
        DataSet ds = new DataSet();
        BLL phongtro = new BLL();
        private void DataBind()
        {
            ds = phongtro.getPhongTro();
            dataGridView1.DataSource = ds.Tables[0];
        }
        public PhongTro()
        {
            InitializeComponent();
        }

        private void PhongTro_Load(object sender, EventArgs e)
        {
            DataBind();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có chắc chắn muốn thoát ?", "Trả lời",
            MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dataGridView1.CurrentCell.RowIndex;
            this.txtMaPhong.Text = dataGridView1.Rows[r].Cells[0].Value.ToString();
            this.txtSoPhong.Text = dataGridView1.Rows[r].Cells[1].Value.ToString();
            this.txtMaLoaiPhong.Text = dataGridView1.Rows[r].Cells[2].Value.ToString();
            this.txtTinhTrang.Text = dataGridView1.Rows[r].Cells[3].Value.ToString();
            this.txtTenLoaiPhong.Text = dataGridView1.Rows[r].Cells[4].Value.ToString();
            this.txtGiaThue.Text = dataGridView1.Rows[r].Cells[5].Value.ToString();
            this.txtNgay.Text= dataGridView1.Rows[r].Cells[6].Value.ToString();
            this.txtMaTB.Text = dataGridView1.Rows[r].Cells[7].Value.ToString();
            this.txtTenTB.Text = dataGridView1.Rows[r].Cells[8].Value.ToString();
            this.txtNgayTrangBi.Text = dataGridView1.Rows[r].Cells[9].Value.ToString();
            this.txtSoLuong.Text = dataGridView1.Rows[r].Cells[10].Value.ToString();
           
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            them = true;
            this.txtMaPhong.ResetText();
            this.txtSoPhong.ResetText();
            this.txtTinhTrang.ResetText();
            this.txtMaLoaiPhong.ResetText();
            this.txtTenLoaiPhong.ResetText();
            this.txtNgay.ResetText();
            this.txtGiaThue.ResetText();
            this.txtMaTB.Enabled = false;
            this.txtTenTB.Enabled = false;
            this.txtNgayTrangBi.Enabled = false;
            this.txtSoLuong.Enabled = false;
            this.btnThem.Enabled = false;
            this.btnLuu.Enabled = true;
            this.txtMaPhong.Focus();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            
            if (them)
            {
                string err = "Lỗi khi thêm !";
                string txtMaPhong = this.txtMaPhong.Text.ToString();
                string txtSoPhong = this.txtSoPhong.Text.ToString();
                string txtMaLoaiPhong = this.txtMaLoaiPhong.Text.ToString();
                string txtTinhTrang = this.txtTinhTrang.Text.ToString();
                string txtTenLoaiPhong = this.txtTenLoaiPhong.Text.ToString();
                string txtNgay = this.txtNgay.Text.ToString();
                DateTime ngay = DateTime.Parse(txtNgay);
                string txtGiaThue = this.txtGiaThue.ToString();

                if (!phongtro.insertPhongTro(ref err, txtMaPhong, txtSoPhong, txtMaLoaiPhong, txtTinhTrang, txtTenLoaiPhong, ngay, txtGiaThue))
                {
                    MessageBox.Show(err);
                }
                else
                {
                    DataBind();
                    MessageBox.Show("Đã thêm xong !");
                }
            }
        }
    }
}
