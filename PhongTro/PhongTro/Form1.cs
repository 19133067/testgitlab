﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLLayer;
using DALayer;
using System.Data;
using System.Data.SqlClient;

namespace PhongTro
{
    public partial class Form1 : Form
    {
        bool them = false;
        DAL da = new DAL();
        DataSet ds = new DataSet();
        BLL khachthue = new BLL();
        public Form1()
        {
            InitializeComponent();
        }
        private void DataBind()
        {
            ds = khachthue.getKhachHang();
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có chắc chắn muốn thoát ?", "Trả lời",
            MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataBind();
            this.btnLuu.Enabled = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dataGridView1.CurrentCell.RowIndex;
            this.txtMaKH.Text = dataGridView1.Rows[r].Cells[0].Value.ToString();
            this.txtHoTen.Text = dataGridView1.Rows[r].Cells[1].Value.ToString();
            this.txtCMND.Text = dataGridView1.Rows[r].Cells[2].Value.ToString();
           
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnChiTietHopDong_Click(object sender, EventArgs e)
        {
            Form frm = new HopDong();
            frm.ShowDialog();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            them = true;
            this.txtCMND.ResetText();
            this.txtHoTen.ResetText();
            this.txtMaKH.ResetText();
            this.txtMaKH.Focus();
            this.btnThem.Enabled = false;
            this.btnLuu.Enabled = true;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            
            if (them)
            {
                string err = "Lỗi khi thêm !";
                string txtMaKH = this.txtMaKH.Text.ToString();
                string txtHoTen = this.txtHoTen.Text.ToString();
                string txtCMND = this.txtCMND.Text.ToString();

                if (!khachthue.insertKhachThue(ref err,txtMaKH,txtHoTen,txtCMND))
                {
                    MessageBox.Show(err);
                }
                else
                {
                    DataBind();
                    MessageBox.Show("Đã thêm xong !");
                }    
            }
            else
            {
                string txtMaKH = this.txtMaKH.Text.ToString();
                string txtHoTen = this.txtHoTen.Text.ToString();
                string txtCMND = this.txtCMND.Text.ToString();

                string err = "Lỗi khi sửu !";
                if (!khachthue.suuKhachThue(ref err, txtMaKH,txtHoTen,txtCMND))
                    MessageBox.Show(err);
                else
                {
                    DataBind();
                    MessageBox.Show("Đã sửu xong !");
                }
            }
            this.btnLuu.Enabled = false;
            this.btnThem.Enabled = true;
        }
        public void Kiemtra()
        {
            // Mở kết nối 
            if (da.conn.State == ConnectionState.Open)
                da.conn.Close();

            da.conn.Open();
            // Thêm dữ liệu 
            try
            {
                // Thực hiện lệnh 
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = da.conn;
                cmd.CommandType = CommandType.Text;
                // Lệnh Kiểm tra RegionID đã tồn tại
                cmd.CommandText = "Select Count(*) From KhachThue Where MaKH='" +
                this.txtMaKH.Text.Trim() + "'";
                int k = Int32.Parse(cmd.ExecuteScalar().ToString());
                if (k > 0)
                {
                    MessageBox.Show("Mã KH tồn tai. Nhập Mã KH khác !");
                    this.txtMaKH.ResetText();
                    this.txtMaKH.Focus();
                }
                else
                {
                    this.txtHoTen.Focus();
                }
            }
            catch (SqlException)
            {
                MessageBox.Show("Lỗi rồi!");
            }
            finally
            {
                da.conn.Close();
            }
        }

        private void txtMaKH_Leave(object sender, EventArgs e)
        {
            Kiemtra();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            them = false;
            this.btnThem.Enabled = false;
            this.btnSua.Enabled = false;
            this.btnLuu.Enabled = true;
            
            // Lấy thông tin của dòng hiện hành
            int r = dataGridView1.CurrentCell.RowIndex;
            this.txtMaKH.Text= dataGridView1.Rows[r].Cells[0].Value.ToString();
            this.txtHoTen.Text = dataGridView1.Rows[r].Cells[1].Value.ToString();
            this.txtCMND.Text = dataGridView1.Rows[r].Cells[2].Value.ToString();
            this.txtMaKH.Enabled = false;
            this.txtHoTen.Focus();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            this.btnLuu.Enabled = false;
            this.btnSua.Enabled = false;
            this.btnThem.Enabled = false;
            string err = "Lỗi khi xóa";
            int r = dataGridView1.CurrentCell.RowIndex;
            string txtMaKH= dataGridView1.Rows[r].Cells[0].Value.ToString();

            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có chắc chắn muốn xóa ?", "Trả lời",
            MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                if (!khachthue.xoaKhachThue(ref err, txtMaKH))
                {
                    MessageBox.Show(err);
                }
                else
                {
                    DataBind();
                    MessageBox.Show("Đã xóa xong !");
                }
            }
            this.txtCMND.ResetText();
            this.txtMaKH.ResetText();
            this.txtHoTen.ResetText();
        }
    }
}
